//var phocusControllers = angular.module('phocusControllers', []);
var phocusControllers = angular.module('phocusControllers', []);
/***************************************
 *Controller leads
 ***************************************/

phocusControllers.controller('IndexController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'Logout', function ($scope, $rootScope, $location, $routeParams, $timeout, Logout) {
        $rootScope.logout = function () {

        }

    }]);
	
phocusControllers.controller('loginController', ['$scope', '$http', function ($scope, $http) {
		$scope.doLogin = function() {
			
			$http({
				url: 'auth/login',
				method: "POST",
				data: {
					'_token' :  $('input[name="_token"]').val(),
					'email'	 :	$('input[name="email"]').val(),
					'password'	 :	$('input[name="password"]').val()
				}
			})
			.then(function(response) {
				console.log(response);
			}, 
			function(response) { // optional
				console.log('Error login request');
			});
			
		}
    }]);

phocusControllers.controller('ContactController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout','ContactRequest', function ($scope, $rootScope, $location, $routeParams, $timeout,ContactRequest) {
        $scope.submitData = {
            full_name: "",
            phone: "",
            email: "",
            subject: "",
            message: "",
            location: "",
            zip_code: "",
            is_provider: "0"
        };
        $scope.saveContactRequest = function () {
            var data = [];
            data.full_name = $scope.submitData.full_name;
            data.subject = $scope.submitData.subject;
            data.email = $scope.submitData.email;
            data.message = $scope.submitData.message;
            data.is_provider = $scope.submitData.is_provider;
            data.location = $scope.submitData.location;
            data.zip_code = $scope.submitData.zip_code;
            ContactRequest.save($scope.submitData, function (response) {
                //$rootScope.success_message = response.message;
            $scope.submitData = {
                full_name: "",
                phone: "",
                email: "",
                subject: "",
                message: "",
                location: "",
                zip_code: "",
                is_provider: ""
            };
            $scope.success_contact="1";
                $location.path('/contact');
            });

        }
    }

    ]);

phocusControllers.controller('AboutController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', function ($scope, $rootScope, $location, $routeParams, $timeout) {

    }]);
phocusControllers.controller('HowController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', function ($scope, $rootScope, $location, $routeParams, $timeout) {

    }]);
phocusControllers.controller('DashboardController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'Dashboard', function ($scope, $rootScope, $location, $routeParams, $timeout, Dashboard) {
        Dashboard.get({}, function (data) {
            $scope.user_details = data;
        });
    }]);
phocusControllers.controller('TermsController', ['$scope', '$rootScope', '$location', '$routeParams', '$timeout', function ($scope, $rootScope, $location, $routeParams, $timeout) {

    }]);
//end by hamza