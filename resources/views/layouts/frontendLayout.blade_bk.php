<!doctype html>
<html ng-app="phocusApp">
    <head>
        <meta charset="utf-8">
        <title>Phocus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="./resources/assets/frontend/images/android-icon-36x36.png">

        <link type="text/css" rel="stylesheet" href="./resources/assets/frontend/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="./resources/assets/frontend/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="./resources/assets/frontend/css/custom.css">
        <link type="text/css" rel="stylesheet" href="./resources/assets/frontend/css/animate.min.css">
        <script type="text/javascript" src="./resources/assets/frontend/js/jquery_2.1.1.js"></script>
        <script type="text/javascript" src="./resources/assets/frontend/js/bootstrap.js"></script>
        <script src="./resources/assets/frontend/app/bower_component/angular/angular.min.js"></script>
        <script src="./resources/assets/frontend/app/bower_component/angular-route/angular-route.js"></script>
        <script src="./resources/assets/frontend/app/bower_component/angular-resource/angular-resource.js"></script>
        <script src="./resources/assets/frontend/app/controllers.js"></script>
        <script src="./resources/assets/frontend/app/app.js"></script>
        <script src="./resources/assets/frontend/app/services.js"></script>
        <script src="./resources/assets/frontend/app/directives.js"></script>
        <script src="./resources/assets/frontend/app/filters.js"></script>
    </head>

	<?php
    	$user = $data['user'];
	?>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 left_to_center">
                        <a href="#/" class="logo wow animated bounceInLeft"><img src="./resources/assets/frontend/images/phocus_logo-47881878d45e7309377ef07bdc1af647.svg"></a>
                    </div>
                    <div class="col-md-9 col-sm-9 right_to_center">
                        <div id="navigation">
                            <span class="toggle"></span>
                            <div class="inner_navigation">
                                <div class="head">
                                    <a href="#"><img src="./resources/assets/frontend/images/phocus_logo-47881878d45e7309377ef07bdc1af647.svg"></a>
                                    <span class="closs"><img src="./resources/assets/frontend/images/close.png"></span>
                                </div>
                                <ul>
                                    <!--<li class="wow animated bounceInDown" data-wow-delay="0.2s"><a href="#/how_it_works">how it works</a></li>-->
                                    <li class="wow animated bounceInDown" data-wow-delay="0.4s"><a href="#/about">about us</a></li>
                                    <li class="wow animated bounceInDown" data-wow-delay="0.4s"><a href="#/contact">contact</a></li>
                                    @if($user)
                                    <li class="wow animated bounceInDown" data-wow-delay="0.4s"><a href="#/dashboard">dashboard</a></li>
                                    <li class="wow animated bounceInDown" data-wow-delay="0.4s"><a href="{{ action('Auth\AuthController@logout')}} " >Logout</a></li>
                                    @else
                                    <li class="wow animated bounceInDown" data-wow-delay="0.6s"><a id="photographer_btn" href="javascript:void(0)">phocus.io pro register</a></li>
                                    <li class="wow animated bounceInDown" data-wow-delay="0.8s">
                                        <a href="javascript:void(0)" id="client_reg">client register</a>
                                    </li>
                                    <li class="wow animated bounceInDown" data-wow-delay="1.0s">
                                        <a href="javascript:void(0)" class="go_to_login_mask">log in</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </header>
        <!--============================================== header end===-->
        
          
         <div  ng-view>
             
             
         </div>
       

        <!--============================================== PopUp===-->




        @include("frontend.dialogs.client_registeration")
        @include("frontend.dialogs.provider_registeration")
        @include("frontend.dialogs.login")
        @include("frontend.dialogs.complete_profile")

        <script type="text/javascript" src="./resources/assets/frontend/js/scripts.js"></script>
        <script type="text/javascript" src="./resources/assets/frontend/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
    </body>
</html>



@if($user && $user->completed_profile==false && $user->role=='Provider')

    <script>
                $('#complete_profile').fadeIn();
    </script>
@endif
