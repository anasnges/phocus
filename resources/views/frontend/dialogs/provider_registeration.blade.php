<div class="mask" id="photographer_mask">
    <div class="container height_100">
        <div class="table_dv height_100">
            <div class="table_cell height_100">
                <div class="provider_register" id="provider_step_1"  style="background: url(./resources/assets/frontend/images/provide_1.jpg); !important;     background-size: cover !important;background-position: 0px !important;">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 color_white right_to_center padding-top-40 height_100">
                            <div class="height_500_auto">
                                <div class="font_27">
                                    Become a Phocus.io Pro Provider to 
                                    take part on the best photography 
                                    projects around your area.
                                </div>
                                <span class="left_absolute"><img src="./resources/assets/frontend/images/point_1.png"></span>
                                <span class="right_absolute">Already a member ? 
                                    <a href="javascript:void(0)" class="bold underline color_white go_to_login_mask">Log in</a></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form_container relative">
                            
                                <span class="mask_close"><img src="./resources/assets/frontend/images/close_2.png" width="32"></span>
                                <h3>PROVIDER REGISTER</h3>
                                <p id="regErrors"></p>
                                
                                <form method="post" id="registrationFrm">
                                    {!! csrf_field() !!}
                                    <div class="margin-bottom-15">
                                        <label>Email</label>
                                        <input type="email" class="form_control" id="emailReg" value="{{ old('email') }}" required />
                                        <span class="help-block regEmailError"></span>
                                    </div>

                                    <div class="margin-bottom-15">
                                        <label>Password</label>
                                        <input type="password" id="passwordReg" class="form_control" required />
                                        <span class="help-block regPwdError"></span>
                                    </div>

                                    <div class="margin-bottom-15">
                                        <label>Password Confirmation</label>
                                        <input type="password" class="form_control" id="cpasswordReg" required/>
                                        <span class="help-block regCPwdError"></span>
                                    </div>

                                    <div class=" text-center">
                                        <input type="submit" class="primary_btn" id="go_to_step_2-t" value="continue" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script language="javascript">
var form = $("#registrationFrm");
    

    form.validate();
$(document).ready(function(e) {
	$("#registrationFrm").submit(function(){
		if($("#passwordReg").val().length < 8){
			$('.regPwdError').html("Password is too short (minimum is 8 characters).");
			$("#passwordReg").focus();
			return false;
		}
		
		if($("#passwordReg").val() != $("#cpasswordReg").val()){
			$('.regCPwdError').html("doesn't match Password.");
			$("#regCPwdError").focus();
			return false;
		}
		
		$.ajax({
		  url: './auth/registration',
		  type: "post",
		  data: {
			  		'email':$("#emailReg").val(), 
					'password':$("#passwordReg").val(), 
					'password_confirmation':$("#cpasswordReg").val(),
					'_token': $('input[name=_token]').val()
		  },
		  success: function(data){
			console.log(data);
			if(parseInt(data) == -1){
				$("#regErrors").html('<div class="alert alert-danger">There is some problem, Please try again later.</div>');
			}
			else if(parseInt(data) == -2){
				$("#regErrors").html('<div class="alert alert-danger">Email address is already taken.</div>');
			}
			else{
				location.reload();
			}
		  }
		  
		});
	});
});
</script>