
<div class="mask" id="client_register_mask">
            <div class="container height_100">
                <div class="table_dv height_100">
                    <div class="table_cell height_100">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">

                                <div class="form_container relative">
                                    <span class="mask_close"><img src="./resources/assets/frontend/images/close_2.png" width="32" /></span>
                                    <h3 class="text-center">REGISTER</h3>
                                    <p class="text-center margin-bottom-20">
                                        This section will soon be available! Leave your email so you and be one of the first to experience Phocus.io
                                    </p>
                                    
                                    <form id="client_registeration" method="post" action='{{url('/register_client')}}'>
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input required name="email" type="email" class="form_control">
                                        </div>
                                        <div class="form-group">
                                            <label>What type of photography work are you looking for? 
</label>
                                            <textarea required="" name="interested_in" class="form_control" ></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 left_to_center">Already a provider? <a href="javascript:void(0)" class="go_to_login_mask">Log In</a></div>
                                                <div class="col-md-6 col-sm-6 right_to_center">
                                                    <input type="submit" class="primary_btn" value="let me know!">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>    
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

<script src="./resources/assets/frontend/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="./resources/assets/frontend/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
     var form = $("#client_registeration");
    

    form.validate();
    
</script>